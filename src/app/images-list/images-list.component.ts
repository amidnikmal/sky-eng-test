import { Component } from '@angular/core';
import { ViewChild, ElementRef } from '@angular/core';
import { OnInit, AfterContentInit } from '@angular/core';

import { Image } from './image';
import { ImagesService } from './images.service';

@Component({
  selector: 'images-list',
  providers: [ ImagesService ],
  templateUrl: './images-list.component.html',
  styleUrls: ['./images-list.component.css']
})
export class ImagesListComponent implements OnInit, AfterContentInit {
  @ViewChild('modal') modal: ElementRef;

  images: Image[] = [];
  currentImage: Image = new Image();

  limit: number;
  start: number;

  constructor(private imagesService: ImagesService) {
    this.limit = 9;
    this.start = 0;
  }

  onImageClick(image) {
    this.currentImage = image;
    this.toggleModal();
  }

  onModalBackgroundClick() {
    console.log('wat??');
    console.log(this.modal);
    //this.toggleModal();
  }

  toggleModal() {
    let modal = this.modal.nativeElement;

    if (modal.style.display === 'block') {
      modal.style.display = 'none';
    } else {
      modal.style.display = 'block';
    }
  }


  get ifPrevActive() {
    if (this.start != 0) {
      return false;
    }

    return true;
  }

  nextClick() {
    this.start += 10;
    this.getImages(this.start, this.limit);
  }

  prevClick() {
    this.start -= 10;
    this.getImages(this.start, this.limit);
  }

  ngOnInit() {
    this.modal.nativeElement.addEventListener('click', () => {
      this.toggleModal();
    });

    document.addEventListener('keydown', (e) => {
      if (e.key === 'Escape') {
        if (this.modal.nativeElement.style.display === 'block') {
          this.toggleModal();
        }
      }
    });

    this.getImages(this.start, this.limit);
  }

  getImages(start, limit): void {
    this.imagesService.getImages(start, limit)
      .subscribe((images) => {
        this.images = this.images.concat(images);
      });
  }
}
