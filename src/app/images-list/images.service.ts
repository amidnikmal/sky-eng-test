import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Image } from './image';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
  }),
};

@Injectable()
export class ImagesService {
  imagesUrl = 'https://jsonplaceholder.typicode.com/photos';

  constructor(private http: HttpClient) { }

  /** GET Images from dummy url **/
  getImages (start, limit): Observable<Image[]> {
    return this.http.get<Image[]>(`${this.imagesUrl}?_start=${start}&_limit=${limit}`)
      .pipe(
        //catchError()
      );
  }
}
