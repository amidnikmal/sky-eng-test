import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';

import { AnotherComponent } from './another/another.component';
import { ImagesListComponent } from './images-list/images-list.component';


@NgModule({
  declarations: [
    AppComponent,
    AnotherComponent,
    ImagesListComponent,
  ],

  imports: [
    BrowserModule,
    HttpClientModule,
  ],

  providers: [],

  bootstrap: [AppComponent]
})

export class AppModule { }
