import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'app';

  baseUrl = 'https://jsonplaceholder.typicode.com/photos';

/*
  getImagesPortion() {
      return this.http.get(this.baseUrl);
  }
  */  
}
